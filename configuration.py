from flask import Flask
from flask_cors import CORS
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy
# from application.routes_init import *

app = Flask(__name__)
api = Api(app)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
db = SQLAlchemy(app)

# db.create_all()

class HelloPost(Resource):
    def post(self):
        return {"message":"message from post"}
class HelloGet(Resource):
    def get(self):
        return {"message": "message from get"}

class HelloWorld(HelloPost, HelloGet):
    def patch(self):
        return {"message": "Success from patch"}

api.add_resource(HelloWorld, '/detail')


