from dataclasses import field
import imp
from flask_restful import Resource, Api, marshal_with, fields
from configuration import db
from models.customer import Customer
from models.customer_region import CustomerRegion
from models.policy import Policy

"""
id = db.Column(db.Integer, primary_key=True)
    premium = db.Column(db.Integer)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    bodyInjuryLiability = db.Column(db.Boolean)
    personalInjuryLiability = db.Column(db.Boolean)
    propertyDamageLiability = db.Column(db.Boolean)
    collision = db.Column(db.Boolean)
    comprehensive = db.Column(db.Boolean)
"""
class DateFormator(fields.Raw):
    def format(self, value):
        return value.strftime("%Y-%m-%dT%H:%M:%SZ")

policy_args = {
    "id": fields.Integer,
    "premium": fields.Integer,
    "date": DateFormator,
    "bodyInjuryLiability": fields.Boolean,
    "personalInjuryLiability": fields.Boolean,
    "propertyDamageLiability": fields.Boolean,
    "collision": fields.Boolean,
    "comprehensive": fields.Boolean,
    "customer": {
        'id': fields.Integer,
        'gender': fields.String,
        'maritalStatus': fields.Integer,
        'regionId': fields.Integer,
        'incomeGroupId': fields.Integer,
    }
}


class PolicyDetail(Resource):
    def get(self):
        data = get_all_policy()
        return {"data": data}
    def post(self):
        policy = Policy(premium=12342, bodyInjuryLiability=False, personalInjuryLiability=True,
                        propertyDamageLiability=True, collision=False, comprehensive=False, customerId=1)
        db.session.add(policy)
        db.session.commit()
        return {"data": policy.date.strftime('%Y-%m-%d')}

@marshal_with(policy_args)
def get_all_policy(**kwargs):
    policies = Policy.query.offset(0).limit(10).all()
    return policies
    
