import imp
from flask_restful import Resource, Api
from configuration import db, api
from models.customer import Customer
from models.customer_region import CustomerRegion

policies = [{}]


class CreateRegion(Resource):
    def post(self):
        region = CustomerRegion(regionDesc="North")
        db.session.add(region)
        db.session.commit()
        return {"data": region.id}


def get_all_policy():
    return policies
