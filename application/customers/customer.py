import imp
from flask_restful import Resource, Api, marshal_with, fields
from sqlalchemy.orm import joinedload
from configuration import db
# from models.customer import Customer
# from models.customer_region import CustomerRegion
# from models.customer_income_group import IncomeGroup
# from models.customer import Customer
from models.user import Post, User

policies = [{}]

resource_fields = {
    'id': fields.Integer,
    'gender': fields.String,
    'maritalStatus': fields.Integer,
    'regionId': fields.Integer,
    'incomeGroupId': fields.Integer,
    'incomeGroup': fields.Nested({
        'income_group_id': fields.Integer,
        'income_group_desc': fields.String
    }),
    # 'region': fields.Nested({
    #     'region_id': fields.Integer,
    #     'region_desc': fields.String
    # }),
}

resource = {
    "id": fields.Integer,
    "title": fields.String,
    "user": fields.Nested({
        "id": fields.Integer,
        "name": fields.String
    })
}


class CustomerDetails(Resource):
    @marshal_with(resource)
    def post(self):
        # customer = Customer(gender="Male", maritalStatus=False,
        #                     regionId=1, incomeGroupId=1) 
        # db.session.add(customer)
        # db.session.commit()
        user = User(name="Prawin")
        db.session.add(user)
        db.session.commit()
        if user is not None:
            post = Post(user_id=user.id, title="Post 2")
            db.session.add(post)
            db.session.commit()
        data = db.session.query(Post).all() # .join(User)
        for p in data:
            print(p.userDetail)
        
        return data

    @marshal_with(resource_fields)
    def get(self):
        # customers = Customer.query.join(IncomeGroup).all()
        # print(customers[0].incomeGroup)
        return "mmm"


def get_all_policy():
    return policies
