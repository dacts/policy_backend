import imp
from flask_restful import Resource, Api
from configuration import db
from models.customer import Customer
from models.customer_region import CustomerRegion
from models.customer_income_group import IncomeGroup

policies = [{}]


class CreateIncomeGroup(Resource):
    def post(self):
        income = IncomeGroup(incomeGroupDesc="$25kto$75k")
        db.session.add(income)
        db.session.commit()
        return {"data": income.id}


def get_all_policy():
    return policies
