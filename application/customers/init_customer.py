from configuration import api
from application.customers.customer import CustomerDetails
# from application.customer.income_group import CreateIncomeGroup
# from application.customer.region import CreateRegion

api.add_resource(CustomerDetails, '/customer')
# api.add_resource(CreateIncomeGroup, '/customer/income-group')
# api.add_resource(CreateRegion, '/customer/region')
