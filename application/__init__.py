from configuration import api

from application.customer import CustomerDetail, RegionDetails
from application.policy import PolicyDetail
from application.summary import Summary

api.add_resource(CustomerDetail, '/customer')
api.add_resource(RegionDetails, '/customer/region')

# Policy
api.add_resource(PolicyDetail, '/policy')

api.add_resource(Summary, '/summary')

