from nis import cat
from flask_restful import Resource, Api, marshal_with, fields, reqparse, abort
from datetime import datetime

from configuration import db
from models.models import Customer, Policy

policy_args = reqparse.RequestParser()
policy_args.add_argument('customerId', type=int, required=True)
policy_args.add_argument('premium', type=int, required=True)
policy_args.add_argument('date', type=lambda x: datetime.strptime(x,'%Y-%m-%dT%H:%M:%SZ'), required=True)
policy_args.add_argument('bodyInjuryLiability', type=bool, required=True)
policy_args.add_argument('personalInjuryLiability', type=bool, required=True)
policy_args.add_argument('propertyDamageLiability', type=bool, required=True)
policy_args.add_argument('collision', type=bool, required=True)
policy_args.add_argument('comprehensive', type=bool, required=True)

policy_patch_args = reqparse.RequestParser()
policy_patch_args.add_argument('policyId', type=int, required=True)
policy_patch_args.add_argument('premium', type=int, required=True)



# policy_list_args.add_argument('page', type=int)
# policy_list_args.add_argument('policyId', type=int)

class DateFormator(fields.Raw):
    def format(self, value):
        return value.strftime("%Y-%m-%dT%H:%M:%SZ")
    
parse_policy_args = {
    "id": fields.Integer,
    "premium": fields.Integer,
    "date": DateFormator,
    "bodyInjuryLiability": fields.Boolean,
    "personalInjuryLiability": fields.Boolean,
    "propertyDamageLiability": fields.Boolean,
    "collision": fields.Boolean,
    "comprehensive": fields.Boolean,
    "customer": fields.Nested({
        'id': fields.Integer,
        'gender': fields.String,
        'maritalStatus': fields.Boolean,
    })
}

class PolicyDetail(Resource):
    def post(self):
        args = policy_args.parse_args()
        customer = db.session.query(Customer).filter(Customer.id == args.customerId).first()
        if customer is None:
            abort(400, message="Customer detail not available")
        policy = Policy(**args)
        db.session.add(policy)
        db.session.commit()
        return policy.id
    
    def patch(self):
        args = policy_patch_args.parse_args()
        policy = Policy.query.filter(Policy.id == args.policyId).first()
        if policy is None:
            abort(400, message="Policy not available")
        policy.premium = args.premium;
        db.session.commit()
        return policy.premium, 200, {'Access-Control-Allow-Origin': '*'} 
    
    @marshal_with(parse_policy_args)
    def get(self):
        args = reqparse.request.args.to_dict()
        print(args)
        policies = addPolicyQuery(args, Policy.query.join(Customer)) 
        return policies, 200, {'Access-Control-Allow-Origin': '*'} 
    
def addPolicyQuery(query, session):
    # print(query)
    for key, v in query.items():
        if key == 'policyId':
            return session.filter(Policy.id.like(f"%{v}")).paginate(page= 1, max_per_page=10).items
    if query.get('page') is not None:
        value = query.get('page')
        try:
            value = int(value)
        except ValueError:
            print("some error")
            value = 1
        return session.paginate(page= value, max_per_page=10).items
    print("One")
    return session.paginate(page= 1, max_per_page=10).items      