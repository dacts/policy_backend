from flask_restful import Resource, Api, marshal_with, fields, reqparse, abort

from configuration import db
from models.models import Customer, Region

customer_args = reqparse.RequestParser()
customer_args.add_argument('gender', type=str, required=True)
customer_args.add_argument('maritalStatus', type=bool, required=True)
customer_args.add_argument('regionId',type=int,required=True)

region_args = reqparse.RequestParser()
region_args.add_argument('name', type=str, required=True)


class CustomerDetail(Resource):
    def post(self):
        args = customer_args.parse_args()
        # gender=args.gender, maritalStatus= args.maritalStatus
        customer = Customer(**args)
        db.session.add(customer)
        db.session.commit()
        return customer.id

class RegionDetails(Resource):
    def post(self):
        args = region_args.parse_args()
        region = Region.query.filter(Region.regionDesc == args.name).first()
        if region is not None:
            abort(400, message="Region already exist.")
        region = Region(regionDesc=args.name)
        db.session.add(region)
        db.session.commit()
        return region.id

    
    
