from flask_restful import Resource, Api, marshal_with, fields, reqparse, abort
from datetime import datetime

from configuration import db
from models.models import Customer, Policy



class Summary(Resource):
    def get(self):
        args = reqparse.request.args.to_dict()
        print(args)
        region = 1
        try:
            region = int(args.get('region'))
        except:
            print("No region")
        policies = Policy.query.join(Customer).filter(Customer.regionId == region).all()
        response = {}
        for policy in policies:
            date = policy.date.strftime('%Y-%m')
            if response.get(date) is not None:
                if policy.customer.gender == "Male":
                    if response[date].get("Male") is not None:
                        response[date]["Male"] += 1
                        continue
                    response[date]["Male"] = 1
                else:
                    if response[date].get("Female") is not None:
                        response[date]["Female"] += 1
                        continue
                    response[date]["Female"] = 1
            else:
                response[date] = {policy.customer.gender: 1}
        return response