from configuration import app, db

import models.models

# Controllers
# from application.policy.init_policy import *
# from application.customers.init_customer import *
import application


def run_app():
    # db.create_all()
    app.run(debug=True)
