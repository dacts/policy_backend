"""
    1. Customer 
        - customer_id
        - customer_gender
        - customer_income_group_id     // Forign key
        - customer_region_id           // Forign key 
        - customer_marital_status
        
    2. Policy 
        - policy_id
        - date_of_purchase
        - body_injury_liability
        - personal_injury_liability
        - property_damage_liability
        - collision
        - comprehensive 
        - premium
        - fuel_type_id                  // Forign key 
        - customer_id                   // Forign key
        - vehicle_segments_id           // Forign key
        
    3. VehicleSegments 
        - vehicle_segments_id
        - vehicle_segment_desc
    4. CustomerRegion 
        - region_id 
        - region_desc   
    5. CustomerIncomeGroup
        - income_group_id
        - income_group_desc
    6. FuelType
        - fuel_type_id
        - fuel_type_desc
    
"""