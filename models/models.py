from configuration import db
from datetime import datetime

class Customer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    gender = db.Column(db.String(10))
    maritalStatus = db.Column(db.Boolean)
    
    #Forign
    regionId = db.Column(db.Integer, db.ForeignKey("region.id"))
    incomeId = db.Column(db.Integer, db.ForeignKey("income.id"))
    #relationship
    policies = db.relationship('Policy', backref='Customer', lazy=False)
    
class Policy(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    premium = db.Column(db.Integer)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    bodyInjuryLiability = db.Column(db.Boolean)
    personalInjuryLiability = db.Column(db.Boolean)
    propertyDamageLiability = db.Column(db.Boolean)
    collision = db.Column(db.Boolean)
    comprehensive = db.Column(db.Boolean)
    
    #Forign key
    customerId = db.Column(db.Integer, db.ForeignKey("customer.id"))
    
    #relationShip
    customer = db.relationship('Customer', back_populates="policies", overlaps="Customer")

class Region(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    regionDesc = db.Column(db.String(50))
    
class Income(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    incomeGroupDesc = db.Column(db.String(50))
    
class Fuel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    incomeGroupDesc = db.Column(db.String(50))
    
class VehicleSegment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    incomeGroupDesc = db.Column(db.String(50))
